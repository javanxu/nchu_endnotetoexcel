#include <File.au3>
#include <Excel.au3>
#include <Array.au3>
#include <Date.au3>


Local $FileList = _FileListToArray(@WorkingDir&"\待轉換的txt檔")
If @error = 1 Then
    MsgBox(0, "執行錯誤", "找不到「待轉換的txt檔」的來源目錄。")
    Exit
EndIf
If @error = 4 Then
    MsgBox(0, "執行錯誤", "「待轉換的txt檔」目錄中沒有待處理的檔案。")
    Exit
EndIf

$excel_row_counter = 0
For $i = 1 To Ubound($FileList)-1
	Local $file = FileOpen(@WorkingDir&"\待轉換的txt檔\"&$FileList[$i], 0)
		If $file = -1 Then
		    MsgBox(0, "執行錯誤", "無法開啟檔案。")
		    Exit
		EndIf

		Local $file_content = FileRead($file)
		If @error = -1 Then ExitLoop
		$file_bib_array = StringSplit($file_content, @CRLF&@CRLF, 1)
		$excel_row_counter = $excel_row_counter + Ubound($file_bib_array)-2
		;_ArrayDisplay($file_bib_array, '')
	FileClose($file)
Next
MsgBox('', '系統訊息', "本次作業共 "&$excel_row_counter-1&" 筆書目，整體去重比對作業預估約需 "&Int(($excel_row_counter*0.018)/60)+1&"分鐘。"&@CRLF&@CRLF&"請按下「OK」鍵後開始進行資料比對。")


Local $all_isbn_array[1]
Local $output_bib_array[1][8]

;$find_counter = 0
$row_counter = 0
$excel_row_counter = 0
For $i = 1 To Ubound($FileList)-1
	Local $file = FileOpen(@WorkingDir&"\待轉換的txt檔\"&$FileList[$i], 0)
		If $file = -1 Then
		    MsgBox(0, "執行錯誤", "無法開啟檔案。")
		    Exit
		EndIf

		Local $file_content = FileRead($file)
		If @error = -1 Then ExitLoop
		$file_bib_array = StringSplit($file_content, @CRLF&@CRLF, 1)
		;_ArrayDisplay($file_bib_array, '')
	FileClose($file)

	$file_name_array = StringSplit($FileList[$i], '-', 1)
	$library_name = $file_name_array[1]

	;Msgbox('','',$library_name)
	For $j = 2 To Ubound($file_bib_array)-1
		Local $field_name_array[8] = ['Title', 'Author', 'Publisher', 'ISBN', 'Year', 'Edition', 'Place Published', 'Library']
		Local $field_output_array[1][8]

		$isbn_code = ''
		$bib_line_array = StringSplit($file_bib_array[$j], @CRLF, 1)
		;_ArrayDisplay($bib_line_array, '')
		If Ubound($bib_line_array) > 3 Then	;判斷行數，夠多才視為該筆書目有內容
			$bib_complate_process_counter = 0
			For $k = 2 To Ubound($bib_line_array)-1
				For $l = 0 To Ubound($field_name_array)-1
					$field_content_array = StringRegExp($bib_line_array[$k], $field_name_array[$l]&": (\w.*)", 1)
					If Ubound($field_content_array)>0 Then
						If $l = 3 Then
							$bib_isbn_array = StringSplit($field_content_array[0], ' ', 1)
							$field_output_array[0][$l] = StringReplace(StringStripWS($bib_isbn_array[1], 3), "-", "")
							$isbn_code = StringReplace(StringStripWS($bib_isbn_array[1], 3), "-", "")
						Else
							$field_output_array[0][$l] = StringStripWS($field_content_array[0], 3)
						EndIf

						$bib_complate_process_counter = $bib_complate_process_counter + 1
						ExitLoop
					EndIf
				Next
				If $bib_complate_process_counter > 8 Then
					ExitLoop
				EndIf
			Next
			;_ArrayDisplay($bib_line_array, '')
		EndIf
		$field_output_array[0][7] = $library_name

		;Msgbox ('','',_ArraySearch($all_isbn_array, $isbn_code))

		If StringInStr($field_output_array[0][0], '[electronic resource]') OR StringInStr($field_output_array[0][0], '[microform]') OR StringInStr($field_output_array[0][0], '[videorecording]') OR StringInStr($field_output_array[0][0], '[Electronic book]') Then
			;$find_counter = $find_counter + 1
		Else
			If _ArraySearch($all_isbn_array, $isbn_code)<0 OR StringLen($isbn_code)=0 Then
				$index = UBound($output_bib_array)
				ReDim $output_bib_array[$index + 1][8]
				For $m = 0 To 7
					$output_bib_array[$excel_row_counter][$m] = $field_output_array[0][$m]
				Next
				$excel_row_counter = $excel_row_counter + 1
			EndIf

			If _ArraySearch($all_isbn_array, $isbn_code)<0 AND StringLen($isbn_code)>=10 Then
				;Msgbox ('','',_ArraySearch($all_isbn_array, $isbn_code))
				_ArrayAdd($all_isbn_array, $isbn_code)
				;_ArrayDisplay($all_isbn_array,'')
			EndIf

			$row_counter = $row_counter + 1
		EndIf
	Next

	FileClose(@WorkingDir&"\待轉換的txt檔\"&$FileList[$i])
	FileMove(@WorkingDir&"\待轉換的txt檔\"&$FileList[$i], @WorkingDir&"\已轉換的txt檔\"&$FileList[$i], 1)
Next

;MsgBox('','',$find_counter)


;_ArrayDisplay($output_bib_array, '')
;_ArrayDisplay($all_isbn_array, '')

MsgBox('', '系統訊息', "去除重覆及含有特殊Title字串後剩 "&$excel_row_counter-1&" 筆書目。"&@CRLF&@CRLF&"輸出至Excel作業預估約需 "&Int(($excel_row_counter*0.17)/60)+1&"分鐘。"&@CRLF&@CRLF&"請按下「OK」鍵後開始進行資料轉換。")

$file_path=''
For $i = 0 To $excel_row_counter-2
	If $i=0 OR Mod($i, 65531)=0 Then
		; 輸出到 Excel ===================================================================================== start
		Local $oExcel = _ExcelBookNew()
		WinSetState ( "活頁簿1 - Excel","",@SW_MINIMIZE)
		$oExcel.Range("A1:I65536").Select
		$oExcel.Selection.NumberFormat = "@"
		_ExcelWriteCell($oExcel, "序號", 1, 1)
		_ExcelWriteCell($oExcel, "範例", 2, 1)
		_ExcelWriteCell($oExcel, "書刊名", 1, 2)
		_ExcelWriteCell($oExcel, "Ideology,Social Theory, and the Environment", 2, 2)
		_ExcelWriteCell($oExcel, "著者", 1, 3)
		_ExcelWriteCell($oExcel, "Sunderlin", 2, 3)
		_ExcelWriteCell($oExcel, "出版者", 1, 4)
		_ExcelWriteCell($oExcel, "Rowman & Little field", 2, 4)
		_ExcelWriteCell($oExcel, "ISBN", 1, 5)
		_ExcelWriteCell($oExcel, "742519708", 2, 5)
		_ExcelWriteCell($oExcel, "出版年", 1, 6)
		_ExcelWriteCell($oExcel, "2002", 2, 6)
		_ExcelWriteCell($oExcel, "版次", 1, 7)
		_ExcelWriteCell($oExcel, "1st ed.", 2, 7)
		_ExcelWriteCell($oExcel, "檢索條件", 1, 8)
		_ExcelWriteCell($oExcel, "subject=public history", 2, 8)
		_ExcelWriteCell($oExcel, "標竿圖書館", 1, 9)
		_ExcelWriteCell($oExcel, "香港中文大學", 2, 9)
	EndIf

	_ExcelWriteCell($oExcel, $i+1, Mod($i, 65531)+3, 1)
	For $j = 0 to 7
		_ExcelWriteCell($oExcel, $output_bib_array[$i][$j], Mod($i, 65531)+3, $j+2)
	Next

	If $i<>0 AND (Mod($i, 65531)=0 OR $i=$excel_row_counter-2) Then
		$file_path = @WorkingDir & "\徵集書單("&$excel_row_counter-1&"筆)_(" & StringReplace(_NowDate(), "/", ".") & "-" & StringReplace(_NowTime(5), ":", ".") & "產製).xls"
		_ExcelBookSaveAs($oExcel, $file_path, "xls", 0, 1)
		_ExcelBookClose($oExcel)
		; 輸出到 Excel ===================================================================================== end
	EndIf
Next

MsgBox('', '系統訊息', "已完成資料轉換!"&@CRLF&@CRLF&"轉出資料存放於以下路徑:"&@CRLF&@CRLF&$file_path)